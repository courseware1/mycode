#! /usr/bin/python3
""" Dumping the data structure from mygame01 out to a JSON data stucture and file """

import json

def main():
    rooms = {

            'Hall' : {
                  'south' : 'Kitchen',
                  'east'  : 'Dining Room',
                  'item'  : 'key'
                },

            'Kitchen' : {
                  'north' : 'Hall',
                  'west'  : 'Pantry',
                  'item'  : 'Haddock'
                },

            'Dining Room' : {
                'west'   :  'Hall',
                'south'  : 'Garden',
                'item'   : 'potion'
                },

            'Garden'  : {
                'north' : 'Dining Room'
                },
            'Pantry'  : {
                'east'  :  'Kitchen',
                'item'  :  'Apple Pie'
                }
         }

    print(rooms)

    ## Now export that data to a JSON file
    with open("mygame01-data.json", "w") as outfile:
        json.dump(rooms, outfile)


if __name__ == "__main__":
    main()

