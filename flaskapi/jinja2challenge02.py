#!/usr/bin/python3

from flask import Flask
from flask import request
from flask import redirect
from flask import url_for
from flask import render_template

app = Flask(__name__)

groups = [{"hostname": "hostA","ip": "192.168.30.22", "fqdn": "hostA.localdomain"},
          {"hostname": "hostB", "ip": "192.168.30.33", "fqdn": "hostB.localdomain"},
          {"hostname": "hostC", "ip": "192.168.30.44", "fqdn": "hostC.localdomain"}]

@app.route("/cathosts")
def supply_hosts():
    # Need a template to render the contents of this data
    return render_template("hostsfile02.html.j2", wallawallawashington = groups)


# GET /addhost?hostname=Larry&ip=10.10.10.10&fqdn=larry.example
@app.route("/addhost")
def addhost():
    # if the hostname exists
    if request.args.get("hostname"):
        newadd = {} # create a blank dict for to add to the group
        newadd["hostname"] = request.args.get("hostname")  # harvest ?hostname=larry
        newadd["ip"] = request.args.get("ip")              # harvest ?ip=10.10.10.10
        newadd["fqdn"] = request.args.get("fqdn")          # harvest ?fqdn=larry.example
        groups.append(newadd)
    return render_template("hostsfile02.html.j2", wallawallawashington = groups)



if __name__ == "__main__":
    app.run(host="0.0.0.0", port=2225)

