#!/usr/bin/python3

import pandas as pd

def main():
    ciscocsv = pd.read_csv("ciscodata.csv")
    ciscojson = pd.read_json("ciscodata2.json")
    
    # display first 5 entries of the ciscocsv dataframe
    print("Displaying the first five lines of the CSV data: ")
    print(ciscocsv.head())

    # display first 5 entries of the ciscojson dataframe            
    print("Displaying the first five lines of the JSON data: ")
    print(ciscojson.head())
    
    ciscodf = pd.concat([ciscocsv, ciscojson])
    # uncomment the line below to "fix" the index issue
    ciscodf = pd.concat([ciscocsv, ciscojson], ignore_index=True, sort=False)
    
    print("Displaying the combined data in a dataframe: ")
    print(ciscodf)
    
if __name__ == "__main__":
    main()

