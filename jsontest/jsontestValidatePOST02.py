#! /usr/bin/python
""" Lesson 26 (requests library - Get vs POST to REST APIs)
     Challenge exercise to validate JSON payload using current time, my IP address and the contents
     of a file 'myservers.txt'                                                                            """

## Import the needed modules
import requests, time, json

## Define URLS to utilise for finding IP addresses, and validating JSON payloads
IPURL = "http://ip.jsontest.com/"
VALIDATEURL = "http://validate.jsontest.com/"

## Define any local variables and files
inputfile = "myservers.txt"


def main():
    # Get the local time for Part A
    timenow = time.localtime()
    PartA = time.strftime("%a, %d-%m-%Y %H:%M:%S %z", timenow)

    print("Part A: " + PartA)
   
    # Get my WAN IP address for Part B
    resp = requests.get(IPURL)
    resppy = resp.json()
    PartB = resppy['ip']

    print('Paart B: ' + PartB)

    # Read the file containing the list of servers for Part C
    PartC = []
    with open(inputfile, "r") as serverfile:
        PartC = serverfile.readlines()

    print('Part C: ' + str(PartC))

    # Construct valid JSON object PartD from the previously created parts (Part D)
    PartD = {}
    Components = {
            'time'    : PartA,
            'ip'      : PartB,
            'mysrvrs' : PartC
            }
    PartD = {'json': Components} 

    print("\n Part D looks like this.... \n")
    print(PartD)
    PartD = json.dumps(PartD)
    print("\n The JSON version of Part D looks like this.... \n")
    print(PartD)

    # Finally attempt to validate my JSON object (Part E)
    resp = requests.post(VALIDATEURL, data=PartD)
    respjson = resp.json()

    print("Testing my JSON via validation URL....\n")
    print(respjson)
    print(f"Is your JSON valid? {respjson['validate']}")


if __name__ == "__main__":
    main()
