#!/usr/bin/python3
"""Alta3 Research | Colm Anderson (2022-04-05)
   Using an HTTP GET to determine when the ISS will pass over head"""

# python3 -m pip install requests
import requests
import json
import time

def main():
    """your code goes below here"""
    
    lssapiroot = 'http://api.open-notify.org/iss-pass.json'

    mylatitude = '53.28546975710686'
    mylongitude = '-6.181979722466979'

    lssarrival = requests.get(f"{lssapiroot}?lat={mylatitude}&lon={mylongitude}")
    lssarrival = lssarrival.json()

    print(lssarrival["request"])
    print(lssarrival["response"])

    # stuck? you can always write comments
    # Try describe the steps you would take manually



if __name__ == "__main__":
    main()

